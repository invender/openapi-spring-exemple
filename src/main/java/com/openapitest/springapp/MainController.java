package com.openapitest.springapp;

import org.openapitest.springlib.api.VersionApi;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MainController implements VersionApi {
    @Override
    public ResponseEntity<String> versionGet() {
        return new ResponseEntity<>("0.0.1", HttpStatus.OK);
    }
}
